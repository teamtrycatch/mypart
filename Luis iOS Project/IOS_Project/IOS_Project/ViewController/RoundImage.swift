//
//  RoundImage.swift
//  IOS_Project
//
//  Created by Xcode User on 2018-04-24.
//  Copyright © 2018 Luis Rios. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func roundImage(){
        
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.borderWidth = 1
        
        //self.layer.cornerRadius = self.frame.size.width / 2
        //self.clipsToBounds = true
    }
}
