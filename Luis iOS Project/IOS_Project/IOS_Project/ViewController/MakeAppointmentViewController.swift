//
//  MakeAppointmentViewController.swift
//  IOS_Project
//
//  Created by Xcode User on 2018-04-24.
//  Copyright © 2018 Luis Rios. All rights reserved.
//

import UIKit

class MakeAppointmentViewController: UIViewController {

    
    @IBOutlet var roundCalendar: UIImageView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var dateNote: UILabel!
    @IBOutlet var hourNote: UILabel!
    
    @IBOutlet var dateTwo: UILabel!
    @IBOutlet var hourTwo: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        
        roundCalendar.roundImage()
    }
    
    @IBAction func pickerValueChange(_ sender: Any) {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, YYYY"
        dateNote.text = formatter.string(from: datePicker.date)
        
        formatter.dateFormat = "hh: mm a"
        hourNote.text = formatter.string(from: datePicker.date)
    }
    
    @IBAction func createAppointment(_ sender: Any) {
        let alert = UIAlertController(title: "Confirm next appointment", message: "Date:  \(dateNote.text!) at \(hourNote.text!),\n YES to confirm or NO for cancel", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "YES", style: .default, handler:{(alert: UIAlertAction!)in
            
            self.dismiss(animated: true, completion: nil)
        })
        
        let noAction = UIAlertAction(title: "NO", style: .cancel, handler: nil)
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        present(alert, animated: true)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, YYYY"
        dateTwo.text = formatter.string(from: datePicker.date)
        
        formatter.dateFormat = "hh: mm a"
        hourTwo.text = formatter.string(from: datePicker.date)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        var scheduleView : ScheduleViewController = segue.destination as! ScheduleViewController
        scheduleView.gotDate = dateNote.text!
        scheduleView.gotHour = hourNote.text!
    }

}
