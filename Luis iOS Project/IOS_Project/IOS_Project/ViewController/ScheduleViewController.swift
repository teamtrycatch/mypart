//
//  ScheduleViewController.swift
//  IOS_Project
//
//  Created by Xcode User on 2018-04-24.
//  Copyright © 2018 Luis Rios. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    @IBOutlet var calendar: UIImageView!
    @IBOutlet weak var activityTableView: UITableView!
    
    @IBOutlet var getHour: UILabel!
    @IBOutlet var getDate: UILabel!
    var gotDate = String()
    var gotHour = String()
    
    var activityDate = ["Mar 14, 2018","Apr 4, 2018", "Apr 18, 2018", "Apr 22, 2018"]
    var activityHour = ["09:30 AM","11:00 AM","4:15 PM","6:00 PM"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        calendar.roundImage()
        
        activityTableView.delegate = self
        activityTableView.dataSource = self
        
        getDate.text = gotDate
        getHour.text = gotHour

        activityDate.insert(getDate.text!, at:4)
        activityHour.insert(getHour.text!, at:4)

        UserDefaults.standard.set(gotDate, forKey: "newDate")
        UserDefaults.standard.set(gotHour, forKey: "newHour")
//        getDate.text = ""
        
    }

    // --- TableView Display arrays --- //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityDate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = activityTableView.dequeueReusableCell(withIdentifier: "cell")
        
        cell?.textLabel?.text = activityDate[indexPath.row]
        cell?.detailTextLabel?.text = "\(activityHour[indexPath.row])"
        return cell!
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let x = UserDefaults.standard.object(forKey: "newDate") as? String {
            getDate.text = x
        }
        if let y = UserDefaults.standard.object(forKey: "newHour") as? String{
            getHour.text = y
        }
    }
    

}
