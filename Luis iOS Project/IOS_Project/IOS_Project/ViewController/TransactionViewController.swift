//
//  TransactionViewController.swift
//  IOS_Project
//
//  Created by Xcode User on 2018-04-25.
//  Copyright © 2018 Luis Rios. All rights reserved.
//

import UIKit

class TransactionViewController: UIViewController {

    @IBOutlet var visa: UIImageView!
    @IBOutlet var mastercard: UIImageView!
    @IBOutlet var americanExpress: UIImageView!
    
    
    @IBOutlet var cardHolder: UITextField!
    @IBOutlet var cardNumber: UITextField!
    @IBOutlet var month: UITextField!
    @IBOutlet var year: UITextField!
    @IBOutlet var secure: UITextField!
    @IBOutlet var payment: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        visa.roundImage()
        mastercard.roundImage()
        americanExpress.roundImage()
    }
    
    @IBAction func paid(_ sender: Any) {
        let alert = UIAlertController(title: "Payment Confirmation", message: "Card holder name:  \(cardHolder.text!) amount of $  \(payment.text!),\n YES to confirm or NO for cancel", preferredStyle: .alert)
        
        let yesAction = UIAlertAction(title: "YES", style: .default, handler:{(alert: UIAlertAction!)in
            self.dismiss(animated: true, completion: nil)
        })
        
        let noAction = UIAlertAction(title: "NO", style: .cancel, handler: nil)
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        present(alert, animated: true)


    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func clearData(_ sender: Any) {
        cardHolder.text = ""
        cardNumber.text = ""
        month.text = ""
        year.text = ""
        secure.text = ""
        payment.text = ""
    }
    
//    func updateNumbers(){
//        randomCardOne = Int(arc4random_uniform(9999))
//        randomCardTwo = Int(arc4random_uniform(9999))
//        randomCardThree = Int(arc4random_uniform(9999))
//        randomCardFour = Int(arc4random_uniform(9999))
//
//        randomMonth = Int(arc4random_uniform(12))
//        randomMonth = Int(arc4random_uniform(24))
//    }

}
